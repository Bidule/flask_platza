# manage.py



# imports
import unittest
from flask_script       import Manager
from project            import create_app, db
from project.api.models import User



# create app and put a Manager on it
app     = create_app()
manager = Manager( app )



# __ 1 __ : launch tests
@manager.command
def test():
    """ Runs the unit tests without test coverage. """
    tests  = unittest.TestLoader().discover( 'project/tests',
                                             pattern='test*.py' ) # search
    result = unittest.TextTestRunner( verbosity=2 ).run( tests )

    if result.wasSuccessful():
        return 0

    return 1



# __ 2 __ : recreate db
@manager.command
def recreate_db():
    """ Recreates a database. """
    db.drop_all()
    db.create_all()
    db.session.commit()



# __ 3 __ : seed
@manager.command
def seed_db():
    """ Seeds the database. """
    db.session.add( User( username='michael', email="michael@realpython.com" ) )
    db.session.add( User( username='michaelherman', email="michael@mherman.org" ) )
    db.session.commit()



if __name__ == '__main__':
    manager.run()
