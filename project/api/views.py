# project/api/views.py



# imports
from flask              import Blueprint, jsonify, request, render_template
from project.api.models import User
from project            import db
from sqlalchemy         import exc



# create a blueprint
users_blueprint = Blueprint( 'users', __name__, template_folder='./templates' )



# __ 1 __ : endpoint = /ping | http = GET | action = print a json
@users_blueprint.route( '/ping', methods=['GET'] )
def ping_pong():
    return jsonify( {
        'status':  'success',
        'message': 'pong!'
    } )



# __ 2 __ : endpoint = /users | http = POST | action = create a user
@users_blueprint.route('/users', methods=['POST'])
def add_user():
    post_data = request.get_json()

    # a_ no data foung => ERR
    if not post_data:
        response_object = {
            'status':  'fail',
            'message': 'Invalid payload.'
        }
        return jsonify( response_object ), 400

    username = post_data.get( 'username' )
    email    = post_data.get( 'email' )

    try:
        user = User.query.filter_by( email=email ).first()

        # b_ add user in db => OK
        if not user:
            db.session.add( User( username=username, email=email ) )
            db.session.commit()
            response_object = {
                'status':  'success',
                'message': f'{email} was added!'
            }
            return jsonify( response_object ), 201
        else: # c_ user already in db => ERR
            response_object = {
                'status':  'fail',
                'message': 'Sorry. That email already exists.'
            }
            return jsonify( response_object ), 400
    # d_ db error => ERR
    except exc.IntegrityError as e:
        db.session.rollback()
        response_object = {
            'status':  'fail',
            'message': 'Invalid payload.'
        }
        return jsonify( response_object ), 400



# __ 3 __ : endpoint = /user/:id | http = GET | action = get single user
@users_blueprint.route( '/users/<user_id>', methods=['GET'] )
def get_single_user( user_id ):
    """ Get single user details """
    response_object = {
        'status':  'fail',
        'message': 'User does not exist'
    }
    try:
        user = User.query.filter_by( id=int( user_id ) ).first()
        # a_ no user found => ERR
        if not user:
            return jsonify( response_object ), 404
        # b_ user found => OK
        else:
            response_object = {
                'status': 'success',
                'data': {
                  'username':   user.username,
                  'email':      user.email,
                  'created_at': user.created_at
                }
            }
            return jsonify( response_object ), 200
    # c_ db error => ERR
    except ValueError:
        return jsonify( response_object ), 404


# __ 4 __ : endpoint = /users | http = GET | action = get all users
@users_blueprint.route( '/users', methods=['GET'] )
def get_all_users():
    """ Get all users """
    users      = User.query.all()
    users_list = []
    for user in users:
        user_object = {
            'id':         user.id,
            'username':   user.username,
            'email':      user.email,
            'created_at': user.created_at
        }
        users_list.append( user_object )
    response_object = {
        'status': 'success',
        'data': {
            'users': users_list
        }
    }
    return jsonify( response_object ), 200


# __ 5 __ endpoint = / | http = GET and POST | action = index
@users_blueprint.route( '/', methods=[ 'GET', 'POST' ] )
def index():
    if request.method == 'POST':
        username = request.form[ 'username' ]
        email = request.form[ 'email' ]
        db.session.add( User( username=username, email=email ) )
        db.session.commit()
    users = User.query.order_by( User.created_at.desc() ).all()
    return render_template( 'index.html', users=users )
